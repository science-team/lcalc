#!/bin/sh
#
# Verify the value of the Riemann zeta function at 0.5 + 100i.
# This is one of the lcalc examples from our documentation.
#
# To accomplish this in POSIX shell, we check the real/imaginary parts
# separately, using awk to print "FAIL" if their actual and expected
# values differ by more than 0.000001.
#

# https://www.wolframalpha.com/input/?i=riemann+zeta+of+%281%2F2+%2B+100*i%29
#
# Put the real/imaginary parts on separate lines so that they can easily
# be compared by compare_fp_list().
EXPECTED="2.6926198856813240904760964705215905770630302273071717661562022
-0.020386029602598161770726853298321520991726471909499988710674"

# See above.
ACTUAL=$("${lcalc}" -v -x .5 -y 100 | sed 's/ /\n/')

printf "Testing the output of lcalc -v -x .5 -y 100... "

. "${testlib}/compare_fp_lists.sh"
compare_fp_lists "${EXPECTED}" "${ACTUAL}"
